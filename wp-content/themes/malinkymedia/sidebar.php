<?php
/**
 * The sidebar.
 */
if ( get_post_type() == 'post' ) { ?>
	<div class="sidebar malinky-fade-in-long-delay" role="complementary">
		<?php dynamic_sidebar( 'sidebar' ); ?>
	</div>
<?php }

if ( get_post_type() == 'post' ) { ?>
	<div class="sidebar-mobile" role="complementary">
		<a class="widget-title heading-3 collapsed" aria-expanded="false">
			<span>Categories and Tags</span><!--
			--><span class="widget-title-toggle__bar"></span><!--
			--><span class="widget-title-toggle__bar"></span>
		</a>
		<div class="sidebar-mobile-inside">
			<?php dynamic_sidebar( 'sidebar-mobile' ); ?>
			<hr />
		</div>
	</div>
<?php } ?>