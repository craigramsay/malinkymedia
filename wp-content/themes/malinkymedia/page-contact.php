<?php
/**
 * Template Name: Page Contact
 *
 * The template for displaying full width pages. Template chosen from admin.
 */

get_header(); ?>

<!-- <main> in header() -->

	<?php while ( have_posts() ) : the_post(); ?>

		<div class="col">
			<div class="col-item">
				<?php get_template_part( 'partials/pages/content', 'contact' ); ?>
			</div>
		</div>

	<?php endwhile; //end loop. ?>

</main><!-- #main -->
	
<?php get_footer(); ?>