<?php
/**
 * The template for displaying all single posts.
 */

get_header(); ?>

<!-- <main> in header() -->

	<div class="col sidebar-flex">

		<div class="col-item col-item-three-quarter--large col-item-three-quarter--xlarge sidebar-flex__content">

		<?php while ( have_posts() ) : the_post(); ?>

			<div class="col">
				<div class="col-item col-item-full">
					<?php get_template_part( 'partials/posts/content', 'single' ); ?>
				</div>
			</div>

			<hr class="hr-40" />

			<div class="col">
				<div class="col-item col-item-full">
					<?php if ( comments_open() || get_comments_number() ) {
						comments_template();
					} ?>
				</div>
			</div>

			<?php malinky_post_pagination(); ?>

		<?php endwhile; //end loop. ?>

		</div><!--
	
		--><div class="col-item col-item-quarter--large col-item-quarter--xlarge sidebar-flex__sidebar">
		
			<?php get_sidebar(); ?>

		</div>

	</div><!-- .col -->

</main><!-- .main -->

<?php get_footer(); ?>