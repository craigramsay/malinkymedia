<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package Malinky Media
 */
?>

<footer class="main-footer wrap" role="contentinfo">

	<div class="col">
		<div class="col-item col-item-3-5--large col-item-3-5--xlarge col-item--align-center--medium col-item--align-center--small main-footer__navigation">
			<div class="col">
				<div class="col-item col-item-half--medium col-item-half--large col-item-half--xlarge main-footer__margin-bottom-40">
					<!-- Contact Us -->
					<h4>CONTACT US</h4>
					<p class="bold-text half-margin" itemprop="telephone"><?php echo esc_html( get_field( 'malinky_settings_contact_phone_number', 'option' ) ); ?></p>
					<p class="bold-text no-margin" itemprop="email"><?php echo esc_html( get_field( 'malinky_settings_contact_email_address', 'option' ) ); ?></p>
				</div><!--
				--><div class="col-item col-item-half--medium col-item-half--large col-item-half--xlarge main-footer__margin-bottom-40">
					<!-- Follow Us Social Media -->
					<h4>FOLLOW US</h4>
					<a href="<?php echo esc_url( get_field( 'malinky_settings_contact_twitter_account', 'option' ) ); ?>" class="image-font image-font__social image-font__twitter" target="_blank"><span class="image-font__fontawesome fa-twitter"></span></a>
					<a href="<?php echo esc_url( get_field( 'malinky_settings_contact_facebook_account', 'option' ) ); ?>" class="image-font image-font__social image-font__facebook" target="_blank"><span class="image-font__fontawesome fa-facebook"></span></a>
					<a href="<?php echo esc_url( get_field( 'malinky_settings_contact_linkedin_account', 'option' ) ); ?>" class="image-font image-font__social image-font__linkedin" target="_blank"><span class="image-font__fontawesome fa-linkedin"></span></a>
				</div>
			</div><!-- .col nested -->
			<div class="col">
				<div class="col-item">
					<!-- Contact Us Address -->
					<h4>FIND US</h4>
					<?php the_field( 'malinky_settings_contact_address', 'option' ); ?>
				</div>
			</div><!-- .col nested -->
			
		</div><!--
		--><div class="col-item col-item-2-5--large col-item-2-5--xlarge col--align-right col-item--align-center--medium col-item--align-center--small">
			<?php echo get_field( 'malinky_settings_contact_company_information', 'option' ); ?>
			<nav class="footer-navigation" role="navigation">
				<?php
		        $args = array(
		            'theme_location'    => 'footer_navigation',
		            'container'   		=> 'false'
		        );
		        ?>
				<?php wp_nav_menu( $args ); ?>
			</nav><!-- .main_navigation -->
			<img src="<?php echo get_template_directory_uri(); ?>/img/logo.svg" class="main-footer__logo" alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>" />
			<p class="main-footer__copyright"><?php echo bloginfo( 'name' ); ?> &copy; <?php echo date('Y'); ?></p>
		</div>
	</div>

</footer><!-- .main-footer -->

<a class="back-top fa-angle-up"></a>

<?php if ( WP_ENV == 'prod' ) { ?>

<!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
<script>
    (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
    function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
    e=o.createElement(i);r=o.getElementsByTagName(i)[0];
    e.src='//www.google-analytics.com/analytics.js';
    r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
    ga('create',"<?php echo esc_html( get_field( 'malinky_settings_contact_google_analytics', 'option' ) ); ?>",'auto');ga('send','pageview');
</script>

<?php } ?>
		
<?php wp_footer(); ?>

</body>
</html>