<?php
/* ------------------------------------------------------------------------ *
 * Template Functions
 * ------------------------------------------------------------------------ */

if ( ! function_exists( 'malinky_is_blog_page' ) ) {

	/**
	 * Check if current page is the blog home page, archive or single.
	 * Archive includes category, tag, date, author pages.
	 * The function excludes CPT to just use native blog/posts.
	 *
	 * @param bool $single Whether to include is_single()
	 *
	 * @return bool
	 */
	function malinky_is_blog_page( $single = true )
	{

	    global $post;

	    $post_type = get_post_type($post);

	    if ( ! $single ) 
	    	return ( ( is_home() || is_archive() ) && ( $post_type == 'post' ) );

	    return ( ( is_home() || is_archive() || is_single() ) && ( $post_type == 'post' ) );

	}

}


if ( ! function_exists( 'malinky_cpt_menu_item_classes' ) ) {

	/**
	 * Add current_page_parent class to Custom Post Type menu items.
	 * Works on a CPT archive, single or custom taxonomy and multiple CPT at once.
	 * Custom taxonomy must be in the CPT.
	 *
	 * Removes current_page_parent class when on one of the above from the Posts page that has been set set in Settings->Reading.
	 *
	 * Thanks to https://gist.github.com/jjeaton/5522014 for a start.
	 *
	 * @param array  $classes CSS classes for the menu item
	 * @param object $item    WP_Post object for current menu item
	 */
	function malinky_cpt_menu_item_classes( $classes, $item )
	{

		/*
		 * Get array of all custom post types, not builtin.
		 */
		$custom_post_types = get_post_types( array( '_builtin' => false ) );

		if ( get_query_var( 'post_type' ) || get_query_var( 'taxonomy' ) ) {
			/*
			 * Remove current_page_parent from the Posts page that has been set in Settings->Reading.
			 * Comparison is done on the post ID.
			 */
			if( $item->object_id == get_option( 'page_for_posts' ) ) {
				$classes = array_diff( $classes, array( 'current_page_parent' ) );
			}

		}

		if ( get_query_var( 'post_type' ) ) {

			/*
			 * Add current_page_parent.
			 * Where the $item->object (which is the post type of the menu item) is in $custom_post_types.
			 * And where $item->object is equal to the current post type.
			 */
			if ( in_array( $item->object, $custom_post_types ) && $item->object == get_query_var( 'post_type' ) ) {
				$classes[] = 'current_page_parent';
			}

		}

		if ( get_query_var( 'taxonomy' ) ) {

			/*
			 * Add current_page_parent.
			 * First where the $item->object (which is the post type of the menu item) is a custom post type.
			 * Then if the current taxonomy is attached to the found custom post type.
			 */		
			if ( $post_type_object = get_post_type_object( $item->object ) ) {
				if ( in_array( get_query_var( 'taxonomy' ), $post_type_object->taxonomies ) ) {
					$classes[] = 'current_page_parent';
				}
			}

		}

		return $classes;

	}

	add_filter( 'nav_menu_css_class', 'malinky_cpt_menu_item_classes', 10, 2 );

}


if ( ! function_exists( 'malinky_acf_image_array' ) ) {

	/**
	 * Output an image URL from ACF that is added as an image_array.
	 * Check for get_field and get_sub_field.
	 *
	 * @param string $malinky_acf_field_name ACF field name
	 * @param string $image_size image_size to output
	 * @return str
	 */
	function malinky_acf_image_array( $malinky_acf_field_name, $image_size = '' )
	{
		$malinky_acf_hero_shot = get_field( $malinky_acf_field_name );
	
		if ( ! $malinky_acf_hero_shot ) {
			$malinky_acf_hero_shot = get_sub_field( $malinky_acf_field_name );
		}

		if ( ! $malinky_acf_hero_shot ) return;

		if ( $image_size ) {
			$malinky_acf_hero_shot = $malinky_acf_hero_shot['sizes'][ $image_size ];
		} else {
			$malinky_acf_hero_shot = $malinky_acf_hero_shot['url'];
		}
		
		return $malinky_acf_hero_shot;
	}

}


if ( ! function_exists( 'malinky_acf_image_array_field' ) ) {

	/**
	 * Output an field from an ACF image_array.
	 * Defaults to the alt tag.
	 * If the alt is empty use a defauly of the site name
	 *
	 * @param string $malinky_acf_field_name ACF field name.
	 * @param string $field field to output from the ACF image_array.
	 * @param string $$field2 field to output from the ACF image_array, for sizes array.
	 * @return str
	 */
	function malinky_acf_image_array_field( $malinky_acf_field_name, $field = 'alt', $field2 = '' )
	{
		$malinky_acf_hero_shot = get_field( $malinky_acf_field_name );
	
		if ( ! $malinky_acf_hero_shot ) {
			$malinky_acf_hero_shot = get_sub_field( $malinky_acf_field_name );
		}

		if ( ! $malinky_acf_hero_shot ) return;
		
		if ( $field == 'alt' ) {
			//No alt tag set.
			if ( $malinky_acf_hero_shot[ $field ] == '' ) {
				return get_bloginfo( 'name' );
			}
		}

		if ( $field2 ) {
			return $malinky_acf_hero_shot[ $field ][ $field2 ];
		} else {
			return $malinky_acf_hero_shot[ $field ];
		}
	}

}


if ( ! function_exists( 'malinky_awp_image' ) ) {

	/**
	 * Output image based on the attachment id, size and return value.
	 * If the attachment size doesn't exist in wordpress returns the original.
	 *
	 * @param int 		$attachment_id 		The attachment id
	 * @param string 	$attachment_size 	The attachment image_size to output
	 * @param string 	$return 			Either url, width or height.
	 * @return str
	 */
	function malinky_wp_image( $attachment_id, $attachment_size = '', $return = 'url' )
	{
		$malinky_attachment = wp_get_attachment_image_src( $attachment_id, $attachment_size );
		
		if ( $malinky_attachment ) {
			switch ( $return ) {
				case 'url':
					return $malinky_attachment[0];
					break;
				case 'width':
					return $malinky_attachment[1];
					break;				
				case 'height':
					return $malinky_attachment[2];
					break;
			}
		}
	}

}


/**
 * Move archive/category count (1) inline as they sit underneath due to links in the sidebar being display: block;
 * @param  str $links The output string of links
 * @return str
 */
/*function malinky_post_count_inline( $links )
{
	$links = preg_replace('/<\/a>.*\(([0-9]+)\)/', ' <span class="cat-count">($1)</span></a>', $links);
	return $links;
}

add_filter( 'get_archives_link', 'malinky_post_count_inline' );
add_filter( 'wp_list_categories', 'malinky_post_count_inline' );
*/