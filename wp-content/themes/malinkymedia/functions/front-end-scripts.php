<?php
/* ------------------------------------------------------------------------ *
 * Front End Scripts
 * ------------------------------------------------------------------------ */

/**
 * Enqueue frontend scripts.
 */
function malinky_scripts()
{

	/* -------------------------------- *
	 * Local && Dev && Prod
	 * -------------------------------- */

	/**
	 * Load WP jQuery and jQuery migrate in the footer.
	 */
	if ( ! is_admin() ) {

		wp_deregister_script( 'jquery' );
		wp_deregister_script( 'jquery-migrate' );

		wp_register_script( 'jquery',
							'/wp-includes/js/jquery/jquery.js',
							false,
							NULL,
							true
		);
		wp_enqueue_script( 'jquery' );

		wp_register_script( 'jquery-migrate',
							'/wp-includes/js/jquery/jquery-migrate.min.js',
							false,
							NULL,
							true
		);
		wp_enqueue_script( 'jquery-migrate' );

	}
	
	
	if ( WP_ENV == 'local' ) {

		/* -------------------------------- *
		 * Local
		 * -------------------------------- */

		/**
		 * Stylesheet which includes normalize.
		 */
		wp_enqueue_style( 'malinky-style', get_stylesheet_uri() );


		/**
		 * Flickity
		 *
		 * @link http://flickity.metafizzy.co/
		 */		
		wp_register_style( 'malinky-flickity-css', 
						   get_template_directory_uri() . '/bower_components/flickity/dist/flickity.css', 
						   false, 
						   NULL
		);
		wp_enqueue_style( 'malinky-flickity-css' );


		/**
		 * Font awesome font.
		 *
		 * @link http://fortawesome.github.io/Font-Awesome/
		 */		
		wp_register_style( 'malinky-animate-css', 
						   get_template_directory_uri() . '/bower_components/animate.css/animate.css', 
						   false, 
						   NULL
		);
		wp_enqueue_style( 'malinky-animate-css' );		


		/**
		 * Font awesome font.
		 *
		 * @link http://fortawesome.github.io/Font-Awesome/
		 */		
		wp_register_style( 'malinky-font-awesome', 
						   '//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css', 
						   false, 
						   NULL
		);
		wp_enqueue_style( 'malinky-font-awesome' );


		/**
		 * Flickity
		 *
		 * @link http://flickity.metafizzy.co/
		 */		
		wp_register_script( 'malinky-flickity-js', 
						   	get_template_directory_uri() . '/bower_components/flickity/dist/flickity.pkgd.js', 
						   	false, 
						   	NULL,
							true
		);
		wp_enqueue_script( 'malinky-flickity-js' );


		/**
		 * jQuery Validate
		 *
		 * @link http://jqueryvalidation.org/
		 */		
		wp_register_script( 'malinky-validate-js', 
						   	get_template_directory_uri() . '/bower_components/jquery-validation/dist/jquery.validate.js', 
						   	array( 'jquery' ), 
						   	NULL,
							true
		);
		wp_enqueue_script( 'malinky-validate-js' );



		/**
		 * Images Loaded
		 *
		 * @link https://github.com/desandro/imagesloaded
		 */		
		wp_register_script( 'malinky-imagesloaded-js', 
						   	get_template_directory_uri() . '/js/imagesloaded.pkgd.js', 
						   	false, 
						   	NULL,
							true
		);
		wp_enqueue_script( 'malinky-imagesloaded-js' );


		/**
		 * Matchmedia polyfill
		 *
		 * @link https://github.com/paulirish/matchMedia.js
		 */		
		wp_register_script( 'malinky-matchmedia-js', 
						   	get_template_directory_uri() . '/js/matchMedia.js', 
						   	false, 
						   	NULL,
							true
		);
		wp_enqueue_script( 'malinky-matchmedia-js' );


		/**
		 * Matchmedia polyfill
		 *
		 * @link https://github.com/paulirish/matchMedia.js
		 */		
		wp_register_script( 'malinky-matchmedialistener-js', 
						   	get_template_directory_uri() . '/js/matchMedia.addListener.js', 
						   	false, 
						   	NULL,
							true
		);
		wp_enqueue_script( 'malinky-matchmedialistener-js' );

		
		/**
		 * Enquire js media queries.
		 *
		 * @link http://fortawesome.github.io/Font-Awesome/
		 */		
		wp_register_script( 'malinky-enquire-js', 
						   	get_template_directory_uri() . '/bower_components/enquire/dist/enquire.min.js', 
						   	false, 
						   	NULL, 
							true
		);
		wp_enqueue_script( 'malinky-enquire-js' );


		/**
		 * Lazy Load
		 *
		 * @link http://www.appelsiini.net/projects/lazyload
		 */
		wp_register_script( 'malinky-lazyload-js',
							get_template_directory_uri() . '/bower_components/jquery.lazyload/jquery.lazyload.js',
							false,
							NULL,
							true
		);
		wp_enqueue_script( 'malinky-lazyload-js' );


		/**
		 * Packery
		 *
		 * @link http://packery.metafizzy.co/
		 */
		wp_register_script( 'malinky-packery-js',
							get_template_directory_uri() . '/js/packery.pkgd.min.js',
							false, 
							NULL,
							true
		);
		wp_enqueue_script( 'malinky-packery-js' );


		/**
		 * Photoswipe.
		 *
		 * @link http://photoswipe.com/
		 */
		wp_register_script( 'malinky-gallery-photoswipe-js',
							get_template_directory_uri() . '/js/photoswipe.js',
							false,
							NULL,
							true
		);
		wp_enqueue_script( 'malinky-gallery-photoswipe-js' );	


		/**
		 * Photoswipe UI.
		 *
		 * @link http://photoswipe.com/
		 */
		wp_register_script( 'malinky-gallery-photoswipe-ui-js',
							get_template_directory_uri() . '/js/photoswipe-ui.js',
							false,
							NULL,
							true
		);
		wp_enqueue_script( 'malinky-gallery-photoswipe-ui-js' );


		/*
		 * Malinky Media related javascript and jQuery.
		 */
		wp_register_script( 'malinky-main-js',
							get_template_directory_uri() . '/js/main.js',
							false,
							NULL,
							true
		);
		wp_enqueue_script( 'malinky-main-js' );

	}


	if ( WP_ENV == 'dev' || WP_ENV == 'prod' ) {

		/* -------------------------------- *
		 * Dev && Prod
		 * -------------------------------- */

		/*
		 * flickity.pkgd.js, imagesloaded.pkgd.js, matchMedia.js, matchMedia.addListener.js, enquire.min.js, jquery.lazyload.js, jquery.validate.js, main.js, packery.pkgd.min.js, photoswipe.js, photoswipe-ui.js
		 */
		wp_register_script( 'malinky-scripts-min-js',
							get_template_directory_uri() . '/js/scripts.min.js',
							array( 'jquery' ),
							NULL,
							true
		);
		wp_enqueue_script( 'malinky-scripts-min-js' );

	}

}

add_action( 'wp_enqueue_scripts', 'malinky_scripts' );


/**
 * Filter to amend the script tags that are loaded.
 * Can change to async for example.
 */
function malinky_javascript_loader( $tag, $handle, $src )
{
	/*
	 * Add array of handles.
	 */
	$async_handles = array();
	
	if ( is_admin() ) return $tag;

	if ( in_array( $handle, $async_handles ) ) {
		$tag = str_replace('src=', 'async src=', $tag);
	}

	return $tag;

}

//add_filter( 'script_loader_tag', 'malinky_javascript_loader', 10, 3 );