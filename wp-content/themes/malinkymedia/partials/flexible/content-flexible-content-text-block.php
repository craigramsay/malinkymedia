<?php echo malinky_acf_hr_header(); ?>
<div class="col<?php echo get_sub_field( 'column_spacing_type' ) == 'padding' || get_sub_field( 'column_spacing_type' ) == 'margin-bottom' ? ' col--' . esc_attr( get_sub_field( 'column_spacing_type' ) ) . '-' . esc_attr( get_sub_field( 'column_spacing_value' ) ) : ''; ?><?php echo get_sub_field( 'narrow' ) ? ' col--align-center' : ''; ?>">
	<div class="col-item<?php echo get_sub_field( 'narrow' ) ? ' col-item-8-10--large col-item-8-10--xlarge' : ''; ?>">
		<?php if ( get_sub_field( 'font_awesome' ) != '' ) { ?>
			<p class="<?php echo esc_attr( get_sub_field( 'font_awesome_alignment' ) ); ?>">
				<span class="image-font__sizing image-font__sizing--large image-font__fontawesome <?php echo esc_attr( get_sub_field( 'font_awesome' ) ); ?> <?php echo esc_attr( get_sub_field( 'font_awesome_color' ) ); ?>"></span>
			</p>
		<?php } ?>
		<?php if ( get_sub_field( 'content' ) != '' ) { ?>
			<span itemprop="mainContentOfPage">
				<?php the_sub_field( 'content' ); ?>
			</span>
		<?php } ?>
	</div>
</div>
<?php echo malinky_acf_hr_footer(); ?>