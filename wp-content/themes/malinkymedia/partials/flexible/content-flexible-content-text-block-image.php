<?php echo malinky_acf_hr_header(); ?>
<div class="col<?php echo get_sub_field( 'column_spacing_type' ) == 'padding' || get_sub_field( 'column_spacing_type' ) == 'margin-bottom' ? ' col--' . esc_attr( get_sub_field( 'column_spacing_type' ) ) . '-' . esc_attr( get_sub_field( 'column_spacing_value' ) ) : ''; ?><?php echo get_sub_field( 'reverse_image_order' ) ? ' col-image-flex' : ''; ?>">
	<div class="col-item col-item-half--large col-item-half--xlarge col--margin-bottom-40--small col--margin-bottom-40--medium<?php echo ' ' . esc_attr( get_sub_field( 'image_alignment' ) ); ?> col-item--middle">
		<img src="<?php echo esc_url( malinky_acf_image_array( 'image' ) ); ?>" alt="<?php echo esc_attr( malinky_acf_image_array_field( 'image' ) ); ?>" itemprop="primaryImageOfPage" />
	</div><!--
	--><div class="col-item col-item-half--large col-item-half--xlarge col-item--middle">
		<?php if ( get_sub_field( 'font_awesome' ) != '' ) { ?>
			<p class="<?php echo esc_attr( get_sub_field( 'font_awesome_alignment' ) ); ?>">
				<span class="image-font__sizing image-font__sizing--large image-font__fontawesome <?php echo esc_attr( get_sub_field( 'font_awesome' ) ); ?> <?php echo esc_attr( get_sub_field( 'font_awesome_color' ) ); ?>"></span>
			</p>
		<?php } ?>
		<?php if ( get_sub_field( 'content' ) != '' ) { ?>
			<span itemprop="mainContentOfPage">
				<?php the_sub_field( 'content' ); ?>
			</span>
		<?php } ?>
	</div>
</div>
<?php echo malinky_acf_hr_footer(); ?>