<?php echo malinky_acf_hr_header(); ?>
<?php $flickitycells = get_sub_field( 'flickity' );
if ( $flickitycells ) {
	shuffle( $flickitycells ); ?>
	<div class="feedback-flickity malinky-fade-in-long-delay">
	<?php foreach ( $flickitycells as $key => $cell ) { ?>
		<div class="feedback-flickity-cell">
			<?php echo $cell[ 'flickity_cell' ]; ?>
		</div>
	<?php } ?>
	</div>
<?php } ?>
<?php echo malinky_acf_hr_footer(); ?>