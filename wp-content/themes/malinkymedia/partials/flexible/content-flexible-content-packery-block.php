<?php echo malinky_acf_hr_header(); ?>

<div id="malinky-gallery-<?php echo rand(1, 100); ?>" class="malinky-gallery packery-container malinky-fade-in-long-delay<?php echo get_sub_field( 'column_spacing_type' ) == 'padding' || get_sub_field( 'column_spacing_type' ) == 'margin-bottom' ? ' col--' . esc_attr( get_sub_field( 'column_spacing_type' ) ) . '-' . esc_attr( get_sub_field( 'column_spacing_value' ) ) : ''; ?>" itemscope itemtype="http://schema.org/ImageGallery">
	<meta itemprop="about" content="<?php echo esc_attr( get_the_title() ); ?> Project Photos by Malinky Media" />
	<div class="packery-gutter"></div>
	<?php 
	$malinkyImageCount = 0;
	while ( have_rows( 'packery_item' ) ) : the_row(); ?>
		<div class="packery-item<?php echo get_sub_field( 'width' ) != 'normal' ? ' ' . esc_attr( get_sub_field( 'width' ) ) : ''; ?>">
			<?php 
			$malinkyImage 			= get_sub_field( 'image' );
			$malinkyImageSize 		= get_sub_field( 'width' ) == 'normal' ? 'mm_projects_400' : 'mm_projects_720';
			$malinkyImageSizeData 	= get_sub_field( 'tall' ) ? 'mm_projects_400_tall' : $malinkyImageSize;
			?>
			<div class="packery-image<?php echo get_sub_field( 'tall' ) ? ' packery-image--tall' : ''; ?>" itemscope itemtype="http://schema.org/ImageObject" data-image-index="<?php echo esc_attr( $malinkyImageCount ); ?>">
				<a href="<?php echo esc_url( $malinkyImage['url'] ); ?>" itemprop="contentUrl image" data-image-size-large="<?php echo esc_attr( $malinkyImage['width'] ); ?>x<?php echo esc_attr( $malinkyImage['height'] ); ?>" data-image-medium="<?php echo esc_url( $malinkyImage['sizes']['mm_projects_960'] ); ?>" data-image-size-medium="<?php echo esc_attr( $malinkyImage['sizes']['mm_projects_960-width'] ); ?>x<?php echo esc_attr( $malinkyImage['sizes']['mm_projects_960-height'] ); ?>" class="malinky-photoswipe-image">
					<img data-original="<?php echo esc_url( malinky_acf_image_array( 'image', $malinkyImageSize ) ); ?>" data-original-malinky="<?php echo esc_attr( $malinkyImageSizeData ); ?>" class="packery-image__img lazy" itemprop="thumbnail" />
					<span class="packery-image__expand"></span>
				</a>
			</div>
		</div>
		<?php $malinkyImageCount++; ?>
	<?php endwhile; ?>
</div>

<?php echo malinky_acf_hr_footer(); ?>