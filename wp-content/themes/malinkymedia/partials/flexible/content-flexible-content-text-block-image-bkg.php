<?php echo malinky_acf_hr_header(); ?>
<div class="col col--gutterless<?php echo get_sub_field( 'column_spacing_type' ) == 'padding' || get_sub_field( 'column_spacing_type' ) == 'margin-bottom' ? ' col--' . esc_attr( get_sub_field( 'column_spacing_type' ) ) . '-' . esc_attr( get_sub_field( 'column_spacing_value' ) ) : ''; ?><?php echo get_sub_field( 'reverse_image_order' ) ? ' col-image-flex' : ''; ?><?php echo ' block-image-bkg block-image-bkg--' . esc_attr( get_sub_field( 'background' ) ); ?>">
	<div class="col-item col-item-4-10--large col-item-4-10--xlarge<?php echo ' ' . esc_attr( get_sub_field( 'image_alignment' ) ); ?> col-item--middle block-image-bkg__image">
		<img src="<?php echo esc_url( malinky_acf_image_array( 'image' ) ); ?>" alt="<?php echo esc_attr( malinky_acf_image_array_field( 'image' ) ); ?>" class="block-image-bkg__image__img" itemprop="primaryImageOfPage" />
	</div><!--	
	--><div class="col-item col-item-6-10--large col-item-6-10--xlarge col-item--middle block-image-bkg__text">
		<?php if ( get_sub_field( 'font_awesome' ) != '' ) { ?>

			<p class="<?php echo esc_attr( get_sub_field( 'font_awesome_alignment' ) ); ?>">
				<span class="image-font__sizing image-font__sizing--large image-font__fontawesome <?php echo esc_attr( get_sub_field( 'font_awesome' ) ); ?> <?php echo esc_attr( get_sub_field( 'font_awesome_color' ) ); ?>"></span>
			</p>
		<?php } ?>
		<?php if ( get_sub_field( 'content' ) != '' ) { ?>
			<span itemprop="mainContentOfPage">
				<?php the_sub_field( 'content' ); ?>
			</span>
		<?php } ?>
	</div>
</div>