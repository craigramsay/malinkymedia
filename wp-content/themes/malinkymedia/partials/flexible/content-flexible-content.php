<?php
/**
 * The template part for displaying ACF flexible content.
 */

if ( have_rows( 'blocks' ) ) :

    while ( have_rows( 'blocks' ) ) : the_row();

        switch ( get_row_layout() ) {

			case 'text_block':

        		include 'content-flexible-content-text-block.php';

			break;

			case 'text_block_half_divider':

				include 'content-flexible-content-text-block-half-divider.php';

			break;

			case 'text_block_half':

				include 'content-flexible-content-text-block-half.php';

			break;	

			case 'text_block_image':

				include 'content-flexible-content-text-block-image.php';

			break;

			case 'text_block_image_bkg':

				include 'content-flexible-content-text-block-image-bkg.php';

			break;

			case 'services_block_fa':

				include 'content-flexible-content-services-fa.php';

			break;

			case 'services_block_image':

				include 'content-flexible-content-services-image.php';

			break;				

			case 'images_block':

				include 'content-flexible-content-images-block.php';

			break;

			case 'projects_block':

				include 'content-flexible-content-projects-block.php';

			break;

			case 'packery_block':

				include 'content-flexible-content-packery-block.php';

			break;

			case 'hr_block':

				include 'content-flexible-content-hr-block.php';

			break;

			case 'shortcode_block':

				echo do_shortcode( get_sub_field( 'shortcode' ) );

			break;

			case 'flickity_block':

				include 'content-flexible-content-flickity-block.php';

			break;			

        }

    endwhile;

else :

endif;

?>