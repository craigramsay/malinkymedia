<?php echo malinky_acf_hr_header(); ?>

<div class="col<?php echo get_sub_field( 'column_spacing_type' ) == 'padding' || get_sub_field( 'column_spacing_type' ) == 'margin-bottom' ? ' col--' . esc_attr( get_sub_field( 'column_spacing_type' ) ) . '-' . esc_attr( get_sub_field( 'column_spacing_value' ) ) : ''; ?>">
	<?php while ( have_rows( 'service' ) ) : the_row(); ?><div class="col-item col-item-half--medium col-item-third--large col-item-third--xlarge col-item--align-center service-fa">
		<a href="<?php echo esc_url( get_sub_field( 'link' ) ); ?>" class="service-fa__image service-fa__image--<?php echo esc_attr( get_sub_field( 'background' ) ); ?>">
			<span class="image-font__sizing image-font__sizing--large image-font__fontawesome <?php echo esc_attr( get_sub_field( 'font_awesome' ) ); ?>"></span>
		</a>
		<?php the_sub_field( 'content' ); ?>
	</div><?php endwhile; ?>
</div>

<?php echo malinky_acf_hr_footer(); ?>