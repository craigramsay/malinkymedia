<?php echo malinky_acf_hr_header(); ?>
<div class="col col--vertical-divider col--narrow<?php echo get_sub_field( 'column_spacing_type' ) == 'padding' || get_sub_field( 'column_spacing_type' ) == 'margin-bottom' ? ' col--' . esc_attr( get_sub_field( 'column_spacing_type' ) ) . '-' . esc_attr( get_sub_field( 'column_spacing_value' ) ) : ''; ?> col--relative">
	<div class="col-item col-item-2-5--large col-item-2-5--xlarge<?php echo get_sub_field( 'text_block_alignment' ) != 'left' ? ' col-item--align-' . esc_attr( get_sub_field( 'text_block_alignment' ) ) . ' col-item--align-left--medium col-item--align-left--small' : ''; ?> block-half-divider">
		<?php if ( get_sub_field( 'font_awesome' ) != '' ) { ?>
			<p class="<?php echo esc_attr( get_sub_field( 'font_awesome_alignment' ) ); ?>">
				<span class="image-font__sizing image-font__sizing--large image-font__fontawesome <?php echo esc_attr( get_sub_field( 'font_awesome' ) ); ?> <?php echo esc_attr( get_sub_field( 'font_awesome_color' ) ); ?>"></span>
			</p>
		<?php } ?>
		<?php if ( get_sub_field( 'content' ) != '' ) { ?>
			<span itemprop="mainContentOfPage">
				<?php the_sub_field( 'content' ); ?>
			</span>
		<?php } ?>
	</div><!--	
	--><div class="col-item col-item-1-5 col-item--align-center col-item--vertical-divider col-item--hide--medium col-item--hide--small">
	</div><!--
	--><div class="col-item col-item-2-5--large col-item-2-5--xlarge">
		<?php if ( get_sub_field( 'font_awesome_2' ) != '' ) { ?>
			<p class="image-font image-font__sizing-large <?php echo esc_attr( get_sub_field( 'font_awesome_alignment_2' ) ); ?>">
				<span class="fa <?php echo esc_attr( get_sub_field( 'font_awesome_2' ) ); ?> <?php echo esc_attr( get_sub_field( 'font_awesome_color_2' ) ); ?>"></span>
			</p>
		<?php } ?>
		<?php if ( get_sub_field( 'content_2' ) != '' ) { ?>
			<span itemprop="mainContentOfPage">
				<?php the_sub_field( 'content_2' ); ?>
			</span>
		<?php } ?>
	</div>
</div>
<?php echo malinky_acf_hr_footer(); ?>