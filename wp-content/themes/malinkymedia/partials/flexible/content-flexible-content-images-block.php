<?php echo malinky_acf_hr_header(); $malinkyImageCount = count( get_sub_field( 'images' ) ); ?>

<div class="col<?php echo get_sub_field( 'column_spacing_type' ) == 'padding' || get_sub_field( 'column_spacing_type' ) == 'margin-bottom' ? ' col--' . esc_attr( get_sub_field( 'column_spacing_type' ) ) . '-' . esc_attr( get_sub_field( 'column_spacing_value' ) ) : ''; ?>">
	<?php while ( have_rows( 'images' ) ) : the_row(); ?><div class="col-item <?php echo $malinkyImageCount == 3 ? ' col-item-third--medium col-item-third--large col-item-third--xlarge' : ' col-item-half--medium col-item-half--large col-item-half--xlarge'; ?> image-block malinky-fade-in-long-delay">
		<?php if ( get_sub_field( 'link' ) ) { ?>
			<a href="<?php echo esc_url( get_sub_field( 'link' ) ); ?>" class="projects__link">
		<?php } ?>
				<div class="image-block__image">
					<img data-original="<?php echo esc_url( malinky_acf_image_array( 'image', 'mm_projects_400' ) ); ?>" class="image-block__image__img lazy" alt="<?php echo esc_attr( get_sub_field( 'header' ) ); ?>" />
					<noscript>
						<img src="<?php echo esc_url( malinky_acf_image_array( 'image', 'mm_projects_400' ) ); ?>" class="image-block__image__img" />
					</noscript>
					<?php if ( get_sub_field( 'link_text' ) ) { ?>
						<div class="image-overlay">
							<div>
								<?php echo esc_html( the_sub_field( 'link_text' ) ); ?>
							</div>
						</div>
					<?php } ?>					
				</div>
		<?php if ( get_sub_field( 'link' ) ) { ?>
			</a>
		<?php } ?>
		<?php the_sub_field( 'text' ) ?>
	</div><?php endwhile; ?>
</div>

<?php echo malinky_acf_hr_footer(); ?>