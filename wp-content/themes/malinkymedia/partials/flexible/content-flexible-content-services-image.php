<?php echo malinky_acf_hr_header(); ?>

<div class="col<?php echo get_sub_field( 'column_spacing_type' ) == 'padding' || get_sub_field( 'column_spacing_type' ) == 'margin-bottom' ? ' col--' . esc_attr( get_sub_field( 'column_spacing_type' ) ) . '-' . esc_attr( get_sub_field( 'column_spacing_value' ) ) : ''; ?>">
	<?php while ( have_rows( 'service' ) ) : the_row(); ?><div class="col-item col-item-third--medium col-item-third--large col-item-third--xlarge service malinky-fade-in-long-delay">
		<div class="service__image">
			<img data-original="<?php echo esc_url( malinky_acf_image_array( 'image', 'mm_projects_400' ) ); ?>" class="service__image__img lazy" alt="<?php echo esc_attr( get_sub_field( 'header' ) ); ?>" />
			<noscript>
				<img src="<?php echo esc_url( malinky_acf_image_array( 'image', 'mm_projects_400' ) ); ?>" class="service__image__img" />
			</noscript>
		</div>
		<?php the_sub_field( 'content' ); ?>
	</div><?php endwhile; ?>
</div>

<?php echo malinky_acf_hr_footer(); ?>