<div class="col" itemscope itemtype="http://schema.org/Blog">
	<?php while ( have_rows( 'project' ) ) : the_row(); ?><div class="col-item col-item-third--medium col-item-third--large col-item-third--xlarge projects malinky-fade-in-long-delay" itemscope itemtype="http://schema.org/BlogPosting">
		<meta itemprop="url" content="<?php echo esc_url( get_permalink() ); ?>" />
		<meta itemprop="name headline" content="<?php echo esc_attr( get_the_title() ) ; ?>" />
		<?php if ( get_sub_field( 'link' ) ) { ?>
			<a href="<?php echo esc_url( get_sub_field( 'link' ) ); ?>" class="projects__link">
		<?php } ?>
				<div class="projects__image">
					<img data-original="<?php echo esc_url( malinky_acf_image_array( 'image', 'mm_projects_400' ) ); ?>" class="projects__image__img lazy" itemprop="image" />
					<noscript>
						<img src="<?php echo esc_url( malinky_acf_image_array( 'image', 'mm_projects_400' ) ); ?>" class="projects__image__img" itemprop="image" />
					</noscript>
					<?php if ( get_sub_field( 'link_text' ) ) { ?>
						<div class="image-overlay">
							<div>
								<?php echo esc_html( the_sub_field( 'link_text' ) ); ?>
							</div>
						</div>
					<?php } ?>
				</div>
		<?php if ( get_sub_field( 'link' ) ) { ?>
			</a>
		<?php } ?>
		<?php the_sub_field( 'text' ) ?>
		<?php echo malinky_content_microdata_footer( true, true, true ); ?>
	</div><?php endwhile; ?>
</div>