<?php echo malinky_acf_hr_header(); ?>
<div class="col col--wide<?php echo get_sub_field( 'column_spacing_type' ) == 'padding' || get_sub_field( 'column_spacing_type' ) == 'margin-bottom' ? ' col--' . esc_attr( get_sub_field( 'column_spacing_type' ) ) . '-' . esc_attr( get_sub_field( 'column_spacing_value' ) ) : ''; ?>">
	<?php while ( have_rows( 'text_block' ) ) : the_row(); ?><div class="col-item col-item-half--medium col-item-half--large col-item-half--xlarge block-half">
		<?php if ( get_sub_field( 'font_awesome' ) != '' ) { ?>
			<p class="<?php echo esc_attr( get_sub_field( 'font_awesome_alignment' ) ); ?>">
				<span class="image-font__sizing image-font__sizing--large image-font__fontawesome <?php echo esc_attr( get_sub_field( 'font_awesome' ) ); ?> <?php echo esc_attr( get_sub_field( 'font_awesome_color' ) ); ?>"></span>
			</p>
		<?php } ?>
		<?php if ( get_sub_field( 'content' ) != '' ) { ?>
			<span itemprop="mainContentOfPage">
				<?php the_sub_field( 'content' ); ?>
			</span>
		<?php } ?>
	</div><?php endwhile; ?>
</div>
<?php echo malinky_acf_hr_footer(); ?>