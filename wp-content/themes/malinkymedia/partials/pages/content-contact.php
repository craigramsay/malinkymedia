<?php
/**
 * The template part for displaying contact page in page-contact.php.
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<?php if ( get_field( 'heading' ) ) { ?>
		<header class="page-header<?php echo get_field( 'narrow' ) ? ' page-header--narrow' : ''; ?>">
			<h1 class="page-header__title" itemprop="name"><?php esc_html( the_field( 'heading' ) ); ?></h1>
		</header><!-- .page-header -->
	<?php } ?>

	<div class="col">
		<div class="col-item">

			<?php if (get_the_content()) { ?>
				<span itemprop="articleBody">
					<?php the_content(); ?>
				</span>
			<?php } ?>

			<?php if ( have_rows( 'blocks' ) ): ?>
				<div class="col">
					<div class="col-item">
						<?php get_template_part( 'partials/flexible/content', 'flexible-content' ); ?>
					</div>
				</div>
			<?php endif; ?>

			<div class="col col--align-center">
				<span itemscope itemtype="http://schema.org/Organization">
					<div class="col-item col-item-third--large col-item-third--xlarge">
						<h3><span class="bold-text">TELEPHONE</span></h3>
						<p class="twice-margin"><span itemprop="telephone"><?php echo esc_html( get_field( 'malinky_settings_contact_phone_number', 'option' ) ); ?></span></p>	
					</div><!--
					--><div class="col-item col-item-third--large col-item-third--xlarge">
						<h3><span class="bold-text">EMAIL ADDRESS</span></h3>
						<p class="twice-margin"><span itemprop="email"><?php echo esc_html( get_field( 'malinky_settings_contact_email_address', 'option' ) ); ?></span></p>
					</div><!--
					--><div class="col-item col-item-third--large col-item-third--xlarge">
						<h3><span class="bold-text">SOCIAL MEDIA</span></h3>
						<a href="<?php echo esc_url( get_field( 'malinky_settings_contact_twitter_account', 'option' ) ); ?>" class="image-font image-font__social image-font__twitter" target="_blank"><span class="image-font__fontawesome fa-twitter"></span></a>
						<a href="<?php echo esc_url( get_field( 'malinky_settings_contact_facebook_account', 'option' ) ); ?>" class="image-font image-font__social image-font__facebook" target="_blank"><span class="image-font__fontawesome fa-facebook"></span></a>
						<a href="<?php echo esc_url( get_field( 'malinky_settings_contact_linkedin_account', 'option' ) ); ?>" class="image-font image-font__social image-font__linkedin" target="_blank"><span class="image-font__fontawesome fa-linkedin"></span></a>
					</div>
				</span>
			</div><!-- .col-nested -->
			<hr class="hr-40" />
		</div>
	</div>

	<?php echo do_shortcode( '[malinky-contact-form]' ); ?>

</article>