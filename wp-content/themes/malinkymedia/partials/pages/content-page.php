<?php
/**
 * The template part for displaying a page in page.php.
 */
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> itemscope itemtype="http://schema.org/ItemPage">

	<?php if ( get_field( 'heading' ) ) { ?>
		<header class="page-header<?php echo get_field( 'narrow' ) ? ' page-header--narrow' : ''; ?>">
			<h1 class="page-header__title" itemprop="name"><?php esc_html( the_field( 'heading' ) ); ?></h1>
		</header><!-- .page-header -->
	<?php } ?>

	<?php if (get_the_content()) { ?>
		<div class="col">
			<div class="col-item">
				<span itemprop="mainContentOfPage">
					<?php the_content(); ?>
				</span>
			</div>
		</div>
	<?php } ?>

	<?php if ( have_rows( 'blocks' ) ): ?>
		<div class="col">
			<div class="col-item">
				<?php get_template_part( 'partials/flexible/content', 'flexible-content' ); ?>
			</div>
		</div>
	<?php endif; ?>

</article><!-- #post-## -->