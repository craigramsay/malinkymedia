<?php
/**
 * The template part for displaying a single post in single.php.
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class( 'content-main' ); ?> itemscope itemtype="http://schema.org/BlogPosting">

	<div class="content-main__image malinky-fade-in-long-delay">
		<?php $post_thumbnail_id = get_post_thumbnail_id( $post->ID ); ?>
    	<img src="<?php echo esc_url( malinky_wp_image( $post_thumbnail_id ) ); ?>" alt="" itempprop="image" />
    </div>

	<header class="content-header">
		<h1 class="content-header__title" itemprop="name headline"><?php the_title(); ?></h1>
		<meta itemprop="url" content="<?php echo esc_url( get_permalink() ); ?>" />
		<div class="content-header__meta">
			<?php echo malinky_content_meta( false, false ); ?><?php echo malinky_content_footer( true, false, false ); ?>
		</div><!-- .content-header__meta -->
	</header><!-- .content-header -->

	<div class="content-main__text">
		<span itemprop="articleBody">
			<?php the_content(); ?>
		</span>
	</div><!-- .content-main -->

	<?php echo malinky_content_microdata_footer( true, false, true ); ?>

</article><!-- #post-## -->