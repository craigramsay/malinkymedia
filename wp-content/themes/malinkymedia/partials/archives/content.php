<?php
/**
 * The template part for displaying lists of posts when no post_format() is set.
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class( 'content-summary malinky-fade-in-long-delay' ); ?> itemscope itemtype="http://schema.org/BlogPosting">

	<div class="col">
		<div class="col-item content-summary__image">
	    	<?php $post_thumbnail_id = get_post_thumbnail_id( $post->ID ); ?>
	        <a href="<?php echo esc_url( get_permalink() ); ?>">
	        	<img src="<?php echo esc_url( malinky_wp_image( $post_thumbnail_id ) ); ?>" alt="<?php echo esc_attr( get_the_title() ); ?>" itemprop="image" />
	        </a>
		</div>
		<div class="col-item">
			<div class="content-summary__text">
				<header class="content-header">
					<h3 class="content-header__title">
						<meta itemprop="url" content="<?php echo esc_url( get_permalink() ); ?>" />
						<a href="<?php esc_url( the_permalink() ); ?>" rel="bookmark" itemprop="name headline">
							<?php the_title(); ?>
						</a>
					</h3>
					<?php if ( get_post_type() == 'post' ) { ?>
						<div class="content-header__meta">
							<?php echo malinky_content_meta( false, false ); ?><?php echo malinky_content_footer( true, false, false ); ?>
						</div><!-- .content-header__meta -->
					<?php } ?>
					<?php the_excerpt();
					printf( '<a href="%1$s" class="content-summary__text__more-link">%2$s</a>', esc_url( get_permalink() ), malinky_read_more_text() ); ?>
				</header><!-- .content-header -->
			</div>
		</div>
	</div><!-- .content-summary -->

	<?php echo malinky_content_microdata_footer( true, false, true ); ?>
	
</article><!-- #post-## -->