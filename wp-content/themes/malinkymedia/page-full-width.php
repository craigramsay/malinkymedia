<?php
/**
 * Template Name: Full Width
 *
 * The template for displaying full width pages. Template chosen from admin.
 */

get_header(); ?>

<!-- <main> in header() -->

	<?php while ( have_posts() ) : the_post(); ?>

		<div class="col">
			<div class="col-item">
				<?php get_template_part( 'partials/pages/content', 'page' ); ?>
			</div>
		</div>

		<?php if ( ( ! is_page( 'work' ) && malinky_tree() == 'work' ) || ( ! is_page( 'services' ) && malinky_tree() == 'services' ) ) malinky_page_pagination(); ?>

	<?php endwhile; //end loop. ?>

</main><!-- #main -->
	
<?php get_footer(); ?>