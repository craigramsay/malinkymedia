<?php
	class WpFastestCachePowerfulHtml{
		private $html = "";
		private $head_html = "";
		private $body_html = "";

		public function __construct(){}

		public function set_html($html){			
			$this->html = $html;
			$this->set_head_html();
			$this->set_body_html();
		}

		public function set_body_html(){
			preg_match("/<body(.+)<\/body>/si", $this->html, $out);
			$this->body_html = $out[0];
		}

		public function set_head_html(){
			preg_match("/<head(.+)<\/head>/si", $this->html, $out);
			$this->head_html = $out[0];
		}

		public function remove_head_comments(){
			$data = $this->head_html;
			$comment_list = array();
			$comment_start_index = false;

			for($i = 0; $i < strlen( $data ); $i++) {
				if(isset($data[$i-3])){
				    if($data[$i-3].$data[$i-2].$data[$i-1].$data[$i] == "<!--"){
						if(!preg_match("/if\s+|endif\s*\]/", substr($data, $i, 17))){
							$comment_start_index = $i-3;
						}
					}
				}

				if(isset($data[$i-2])){
					if($comment_start_index){
						if($data[$i-2].$data[$i-1].$data[$i] == "-->"){
							array_push($comment_list, array("start" => $comment_start_index, "end" => $i));
							$comment_start_index = false;
						}
					}
				}
			}

			if(!empty($comment_list)){
				foreach (array_reverse($comment_list) as $key => $value) {
					$data = substr_replace($data, '', $value["start"], ($value["end"] - $value["start"] + 1));
				}

				$this->html = str_replace($this->head_html, $data, $this->html);
			}
			

			// $ini = 0;

			// if(function_exists("ini_set") && function_exists("ini_get")){
			// 	$ini = ini_get("pcre.recursion_limit");
			// 	ini_set("pcre.recursion_limit", "2777");
			// }

			// if($new_head = preg_replace("/<!--((?:(?!-->|endif).)+)-->/si", '', $this->head_html)){
			// 	$this->html = str_replace($this->head_html, $new_head, $this->html);
			// }

			// if($ini){
			// 	ini_set("pcre.recursion_limit", $ini);
			// }

			return $this->html;
		}

		public function minify_html(){
			if(function_exists("ini_set") && function_exists("ini_get")){
				$ini = ini_get("pcre.recursion_limit");
				ini_set("pcre.recursion_limit", "2777");
			}

			//$new_body = $this->remove_single_line_comments($this->body_html);

			$new_body = $this->body_html;

			// $new_body = preg_replace("/<script([^\>\<]*)>\s*/i", "<script$1>", $new_body);
			// $new_body = preg_replace("/\s*<\/script>/i", "</script>", $new_body);

			$this->html = str_replace($this->body_html, $new_body, $this->html);

			$inc = 5000;
			$tmp_html = "";
			$html_part = "";
			for ($i=0; $i < strlen($this->html); $i = $i + $inc) { 
				$html_part = substr($this->html, $i, $inc);
				
				if($html_part_opt = preg_replace_callback("/<div[^\>]*>((?:(?!div|script|style).)+)<\/div>/is", array($this, 'eliminate_newline'), $html_part)){
					$html_part = $html_part_opt;
				}

				$tmp_html = $tmp_html.$html_part;

				// $tmpi = @strpos($this->html, '<div', $i+$inc);

				// if($tmpi){
				// 	$tmp_html .= substr($this->html, ($i+$inc), ($tmpi-$inc));

				// 	if(($i+$tmpi) < strlen($this->html)){
				// 		$i = $tmpi - $inc;
				// 	}else{
				// 		break;
				// 	}
				// }
			}

			$tmp_html = $this->minify_inline_js($tmp_html);
			$tmp_html = $this->minify_inline_css($tmp_html);

			$tmp_html = $this->remove_html_comments($tmp_html);

			$tmp_html = preg_replace("/\s*<\/div>\s*/is", "</div>", $tmp_html);

			$this->html = $tmp_html;

			if($ini){
				ini_set("pcre.recursion_limit", $ini);
			}

			return $this->html;
		}

		public function eliminate_newline($matches){
			return preg_replace("/\s+/", " ", ((string) $matches[0]));
		}

		public function remove_single_line_comments($html){
			$html = preg_replace("/<!--((?:(?!-->).)+)-->/", '', $html);
			$html = preg_replace("/\/\*((?:(?!\*\/).)+)\*\//", '', $html);
			return $html;
		}

		public function remove_html_comments($data){
			$comment_list = array();
			$comment_start_index = false;

			for($i = 0; $i < strlen( $data ); $i++) {
				if(isset($data[$i-3])){
				    if($data[$i-3].$data[$i-2].$data[$i-1].$data[$i] == "<!--"){
						if(!preg_match("/if\s+|endif\s*\]/", substr($data, $i, 17))){
							$comment_start_index = $i-3;
						}
					}
				}

				if(isset($data[$i-2])){
					if($comment_start_index){
						if($data[$i-2].$data[$i-1].$data[$i] == "-->"){
							array_push($comment_list, array("start" => $comment_start_index, "end" => $i));
							$comment_start_index = false;
						}
					}
				}
			}

			if(!empty($comment_list)){
				foreach (array_reverse($comment_list) as $key => $value) {
					if(($value["end"] - $value["start"]) > 4){
						$comment_html = substr($data, $value["start"], ($value["end"] - $value["start"] + 1));

						if(preg_match("/google\_ad\_slot/i", $comment_html)){
						}else{
							$data = substr_replace($data, '', $value["start"], ($value["end"] - $value["start"] + 1));
						}
					}
				}
			}

			return $data;
		}
		/* CSS Part Start */
		public function minify_css($source){
			$data = $source;
			$curl_list = array();
			$curl_start_index = false;

			$curl_start_count = 0;
			$curl_end_count = 0;

			for($i = 0; $i < strlen( $data ); $i++) {
				if($data[$i] == "{"){
					$curl_start_count++;
					if(!$curl_start_index){
						$curl_start_index = $i;
					}
				}

				if($data[$i] == "}"){
					$curl_end_count++;
				}

				if($curl_start_count && $curl_start_count == $curl_end_count){
					array_push($curl_list, array("start" => $curl_start_index-3, "end" => $i+3));

					$curl_start_count = 0;
					$curl_end_count = 0;
					$curl_start_index = false;
				}
			}

			if(!empty($curl_list)){
				foreach (array_reverse($curl_list) as $key => $value) {
					$new_data = substr($data, $value["start"], ($value["end"] - $value["start"] + 1));

					if(!preg_match("/[^\{]+\{[^\{]+\{/", $new_data)){
						$new_data = preg_replace("/\s+/", " ", ((string) $new_data));
						$new_data = preg_replace("/\s*{\s*/", "{", $new_data);
						$new_data = preg_replace("/\s*}\s*/", "}\n", $new_data);
						$new_data = preg_replace("/\s*\;\s*/", ";", $new_data);
						$new_data = preg_replace("/\s*\:\s*/", ":", $new_data);

						$data = substr_replace($data, $new_data, $value["start"], ($value["end"] - $value["start"] + 1));

					}else{
						$first = strpos($new_data, "{");
						$last = strrpos($new_data, "}");
						$new_data_tmp = substr($new_data, $first+1, $last-$first-1);
						$new_data_tmp = $this->minify_css($new_data_tmp);

						$new_data = substr_replace($new_data, $new_data_tmp, $first+1, ($last-$first-1));

						$data = substr_replace($data, $new_data, $value["start"], ($value["end"] - $value["start"] + 1));
					}
				}

				$source = $data;
			}

			return $source;

			//$source = preg_replace_callback("/\s*\{((?:(?!content|\}).)+)\}\s*/", array($this, 'eliminate_newline_for_css'), $source);
			//return $source;
		}
		public function eliminate_newline_for_css($matches){
			$matches[0] = preg_replace("/\s+/", " ", ((string) $matches[0]));
			$matches[0] = preg_replace("/\s*{\s*/", "{", $matches[0]);
			$matches[0] = preg_replace("/\s*}\s*/", "}", $matches[0]);
			$matches[0] = preg_replace("/\s*\;\s*/", ";", $matches[0]);
			$matches[0] = preg_replace("/\s*\:\s*/", ":", $matches[0]);

			return $matches[0]."\n";
		}
		public function minify_inline_css($data){
			$tmp_html = $data;
			$style_list = array();
			$style_start_index = false;

			for($i = 0; $i < strlen( $data ); $i++) {
				if(isset($data[$i-5])){
				    if(substr($data, $i-5, 6) == "<style"){
				    	$style_start_index = $i-5;
					}
				}

				if(isset($data[$i-7])){
					if($style_start_index){
						if(substr($data, $i-7, 8) == "</style>"){
							array_push($style_list, array("start" => $style_start_index, "end" => $i));
							$style_start_index = false;
						}
					}
				}
			}

			if(!empty($style_list)){
				foreach (array_reverse($style_list) as $key => $value) {
					$inline_style = substr($data, $value["start"], ($value["end"] - $value["start"] + 1));
					$inline_style = $this->minify_css($inline_style);
					$data = substr_replace($data, $inline_style, $value["start"], ($value["end"] - $value["start"] + 1));

				}
			}

			return $data;
		}
		/* CSS Part Start */

		/* Js Part Start */
		public function minify_js($source, $inline_js = false){
			//$source = preg_replace("/\n\/\/.*/", "", $source);
			//$source = preg_replace("/\/\*.*?\*\//s", "", $source);

			if(preg_match("/dynamicgoogletags\.update\(\)/i", $source)){
				$source = "<script>dynamicgoogletags.update();</script>";
				
				return $source;
			}

			if(preg_match("/GoogleAnalyticsObject|\_gaq\.push\(\[\'\_setAccount/i", $source)){
				$source = preg_replace("/\s+/", " ", ((string) $source));
				$source = preg_replace("/\s*<(\/?)script([^\>]*)>\s*/", "<$1script$2>", $source);

				return $source;
			}

			$source = preg_replace("/^\s+/m", "", $source);
			
			$source = preg_replace_callback("/\/\*(.*?)\*\//s", array($this, 'remove_multi_line_comments_from_js'), $source);

			if(!$inline_js){
				// // --></script> in html
				$source = preg_replace("/\n\/\/[^\n]+/", "", $source); // to remove single line comments
			}

			$source = preg_replace_callback("/([a-z]{4,5}\:)?\/\/[^\n]+/", array($this, 'remove_single_line_comments_from_js'), $source);

			$source = preg_replace("/\}\)\;\s+/", "});", $source);

			$source = preg_replace("/^\s+/m", "", $source);


			$source = preg_replace("/\s*(\!|\=)(\={1,3})\s*/", "$1$2", $source);
			$source = preg_replace("/\s*\:\s*/", ":", $source);
			$source = preg_replace("/\s*\=\s*/", "=", $source);
			$source = preg_replace("/\)\s+\{/", "){", $source);
			// $source = preg_replace("/;\s*}\s*/s", ";}", $source);
			$source = preg_replace("/\}\s+}/s", "}}", $source);
			$source = preg_replace("/\};\s+}/s", "};}", $source);
			$source = preg_replace("/\}\s*else\s*\{/", "}else{", $source);
			$source = preg_replace("/if\s*\(\s*/", "if(", $source);
			//$source = preg_replace("/\(\s+/", "(", $source); // causes an issue
			$source = preg_replace("/\s+\)/", ")", $source);

			$source = preg_replace("/<script([^\>\<]*)>\s*/i", "<script$1>", $source);
			$source = preg_replace("/\s*<\/script>/i", "</script>", $source);


			return $source;
		}

		public function remove_multi_line_comments_from_js($matches){
			if(preg_match("/\.exec\(|\.test\(|\.match\(|\.search\(|\.replace\(|\.split/", $matches[0])){
				return $matches[0];
			}

			if(preg_match("/function\(/", $matches[0])){
				return $matches[0];
			}

			return "";
		}

		public function remove_single_line_comments_from_js($matches){

			// if (URL.match( /^https?:\/\// ) ) {
			if(preg_match("/^\/\/\s*\)\s*\)\s*\{/", $matches[0])){
				return $matches[0];
			}

			// "string".replace(/\//,3);
			if(preg_match("/^\/\/\s*\,/", $matches[0])){
				return $matches[0];
			}

			// comments: /\/\*[^*]*\*+([^/][^*]*\*+)*\//gi,
			if(preg_match("/^\/\/\s*gi\s*\,/", $matches[0])){
				return $matches[0];
			}
			
			if(preg_match("/^\/\/\//", $matches[0])){
				return $matches[0];
			}
			
			if(preg_match("/^http/", $matches[0])){
				return $matches[0];
			}

			if(preg_match("/\.exec\(|\.test\(|\.match\(|\.search\(|\.replace\(|\.split/", $matches[0])){
				return $matches[0];
			}

			if(preg_match("/^\/\/(?!\-)(?:[a-zA-Z\d\-]{0,62}[a-zA-Z\d]\.){1,126}(?!\d+)[a-zA-Z\d]{1,63}/", $matches[0])){
				return $matches[0];
			}

			if(preg_match("/\'|\"/", $matches[0])){
				// var _emptyPage='//about:blank', content="john's page";
				if(preg_match("/[\'|\"]\s*\,/", $matches[0])){
					return $matches[0];
				}

				// ' something
				if(preg_match("/^\/\/\s*[\'|\"]/", $matches[0])){
					return $matches[0];
				}

				/*function Uc(a,b){var c=Q&&Q.isAvailable(),d=c&&!(nb.kd||!0===nb.get("previous_websocket_failure"));b.ce&&(c||L("
				wss:// URL used, but browser isn't known to support websockets.  Trying anyway."),d=!0);if(d)a.Mb=[Q];else{var e=a.Mb=[];Vb(Vc,function(a,b){b&&b.isAvailable()&&e.push(b)})}}function Wc(a){if(0<a.Mb.length)return a.Mb[0];throw Error("No transports available");};function Xc(a,b,c,d,e,f){this.id=a;this.e=Mb("c:"+this.id+":");this.Lc=c;this.Ab=d;this.S=e;this.Kc=f;this.M=b;this.fc=[];this.Zc=0;this.yd=new Tc(b);this.ma=0;this.e("Connection created");Yc(this)}
				*/
				if(preg_match("/if\(/", $matches[0]) && preg_match("/this\./", $matches[0]) && preg_match("/function/", $matches[0])){
					return $matches[0];
				}

				return "";
			}

			if(preg_match("/<\/script>/", $matches[0])){
				return preg_replace("/\/\/[^\<]+<\/script>/", "</script>", $matches[0]);
			}

			return "";
		}

		public function minify_inline_js($data){
			$tmp_html = $data;
			$script_list = array();
			$script_start_index = false;

			for($i = 0; $i < strlen( $data ); $i++) {
				if(isset($data[$i-6])){
				    if(substr($data, $i-6, 7) == "<script"){
				    	$script_start_index = $i-6;
					}
				}

				if(isset($data[$i-8])){
					if($script_start_index){
						if(substr($data, $i-8, 9) == "</script>"){
							array_push($script_list, array("start" => $script_start_index, "end" => $i));
							$script_start_index = false;
						}
					}
				}
			}

			if(!empty($script_list)){
				foreach (array_reverse($script_list) as $key => $value) {
					$inline_script = substr($data, $value["start"], ($value["end"] - $value["start"] + 1));

					if(preg_match("/google\_ad\_slot/i", $inline_script)){
						continue;
					}
						
					$inline_script = $this->minify_js($inline_script, true);

					$data = substr_replace($data, $inline_script, $value["start"], ($value["end"] - $value["start"] + 1));

				}
			}

			return $data;
		}

		public function minify_js_in_body($wpfc){
			$data = $this->html;
			$script_list = array();
			$script_start_index = false;

			for($i = 0; $i < strlen( $data ); $i++) {
				if(isset($data[$i-6])){
				    if(substr($data, $i-6, 7) == "<script"){
				    	$script_start_index = $i-6;
					}
				}

				if(isset($data[$i-8])){
					if($script_start_index){
						if(substr($data, $i-8, 9) == "</script>"){
							array_push($script_list, array("start" => $script_start_index, "end" => $i));
							$script_start_index = false;
						}
					}
				}
			}

			if(!empty($script_list)){
				foreach (array_reverse($script_list) as $key => $value) {
					$script_tag = substr($data, $value["start"], ($value["end"] - $value["start"] + 1));

					if(preg_match("/^<script[^\>\<]+src\=[^\>\<]+>/i", $script_tag) && !preg_match("/\/wpfc\-minified\//i", $script_tag)){

						preg_match("/src\=[\"\']([^\'\"]+)[\"\']/i", $script_tag, $src);

						$http_host = str_replace(array("http://", "www."), "", $_SERVER["HTTP_HOST"]);

						if(preg_match("/".preg_quote($http_host, "/")."/i", $src[1])){

							$cachFilePath = WPFC_WP_CONTENT_DIR."/cache/wpfc-minified/".md5($script_tag);
							$jsScript = content_url()."/cache/wpfc-minified/".md5($script_tag);
							$jsScript = str_replace(array("http://", "https://"), "//", $jsScript);

							$response = wp_remote_get($src[1], array('timeout' => 10 ) );

							if ( !$response || is_wp_error( $response ) ) {
								continue;
							}else{
								if(wp_remote_retrieve_response_code($response) == 200){
									$js_content = wp_remote_retrieve_body( $response );

									if(preg_match("/<\/\s*html\s*>\s*$/i", $js_content)){
										continue;
									}else{
										$minified_js_content = $this->minify_js($js_content);

										if(!is_dir($cachFilePath)){
											$prefix = time();
											$wpfc->createFolder($cachFilePath, $minified_js_content, "js", $prefix);
										}

										if($jsFiles = @scandir($cachFilePath, 1)){
											$new_script = str_replace($src[1], $jsScript."/".$jsFiles[0], $script_tag);
											$this->html = substr_replace($this->html, $new_script, $value["start"], ($value["end"] - $value["start"] + 1));
										}
									}
								}
							}
						}
					}
				}
			}

			return $this->html;
		}

		public function combine_js_in_footer($CreateCache, $minify){
			$footer = strstr($this->html, '<!--WPFC_FOOTER_START-->');

			$js = new JsUtilities($CreateCache, $footer, $minify);

			$tmp_footer = $js->combine_js();

			$this->html = str_replace($footer, $tmp_footer, $this->html);

			return $this->html;
		}
		/* Js Part End */
	}
?>